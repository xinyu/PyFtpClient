#! /usr/bin/python

######################################################################
#
#      Project: FtpClient
#      Version: 1.0
#         Date: Sat Jul 10 00:00:00 2010
#       Author: Lin Xin Yu
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: 
#               A PyQt based ftp client program
#
######################################################################

############### import modules #######################################

import sys
import FtpClientClass
from PyQt4 import QtCore, QtGui

############### FtpClient start ######################################

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	FtpClientObj = FtpClientClass.FtpClientClass()
	FtpClientObj.show()
	sys.exit(app.exec_())

############### FtpClient start ######################################
