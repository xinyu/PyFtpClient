#! /usr/bin/python

######################################################################
#
#      Project: FtpClient
#      Version: 1.0
#         Date: Sat Jul 10 00:00:00 2010
#       Author: Lin Xin Yu
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: 
#               A PyQt based ftp client program
#
######################################################################

############### import modules #######################################

import os
from PyQt4 import QtCore, QtGui

############### Ui_FtpClientClass start ##############################

class Ui_FtpClientClass(object):
	def setupUi(self, FtpClientClass):
		# MainWindow Widget
		FtpClientClass.setObjectName("FtpClientClass")
		FtpClientClass.resize(QtCore.QSize(QtCore.QRect(0,0,800,500).size()).expandedTo(FtpClientClass.minimumSizeHint()))
		self.moveToCenter(FtpClientClass)
		
		# Centeral Widget for containing all widgets except for menubar and statusbar
		self.centralwidget = QtGui.QWidget(FtpClientClass)
		self.centralwidget.setObjectName("centralwidget")
		
		# Ftp Host label
		self.label_address = QtGui.QLabel(self.centralwidget)
		self.label_address.setGeometry(QtCore.QRect(10,10,70,25))
		self.label_address.setObjectName("label_address")

		# Ftp Host input
		self.lineEdit_address = QtGui.QLineEdit(self.centralwidget)
		self.lineEdit_address.setGeometry(QtCore.QRect(80,10,510,25))
		self.lineEdit_address.setObjectName("lineEdit_address")
		
		# Port Label
		self.label_port = QtGui.QLabel(self.centralwidget)
		self.label_port.setGeometry(QtCore.QRect(600,10,30,25))
		self.label_port.setObjectName("label_port")

		# Port input
		self.lineEdit_port = QtGui.QLineEdit(self.centralwidget)
		self.lineEdit_port.setGeometry(QtCore.QRect(640,10,50,25))
		self.lineEdit_port.setObjectName("lineEdit_port")
		
		# Ftp tranfer mode
		self.transferMode = QtGui.QComboBox(self.centralwidget)
		self.transferMode.setGeometry(QtCore.QRect(700,10,90,25))
		self.transferMode.insertItem(0, 'Passive')
		self.transferMode.insertItem(1, 'Active')		
		
		# Username label
		self.label_username = QtGui.QLabel(self.centralwidget)
		self.label_username.setGeometry(QtCore.QRect(10,40,70,25))
		self.label_username.setObjectName("label_username")

		# Username input
		self.lineEdit_username = QtGui.QLineEdit(self.centralwidget)
		self.lineEdit_username.setGeometry(QtCore.QRect(80,40,270,25))
		self.lineEdit_username.setObjectName("lineEdit_username")
 
 		# Password label
		self.label_password = QtGui.QLabel(self.centralwidget)
		self.label_password.setGeometry(QtCore.QRect(360,40,70,25))
		self.label_password.setObjectName("label_password")

		# Password input
		self.lineEdit_password = QtGui.QLineEdit(self.centralwidget)
		self.lineEdit_password.setGeometry(QtCore.QRect(430,40,260,25))
		self.lineEdit_password.setObjectName("lineEdit_password")
		self.lineEdit_password.setEchoMode(QtGui.QLineEdit.Password)
       
      # Connect/Disconnect button  
		self.pushButton_connect = QtGui.QPushButton(self.centralwidget)
		self.pushButton_connect.setGeometry(QtCore.QRect(700,40,90,25))
		self.pushButton_connect.setObjectName("pushButton_connect")
		
		# TreeWidget for local file system
		self.sysTree = QtGui.QTreeWidget(self.centralwidget)
		self.sysTree.setGeometry(QtCore.QRect(400,75,390,400))
		self.sysTree.setHeaderLabels(['Name', 'Size', 'Owner', 'Group','Path']);
		self.sysTree.setObjectName("sysTree")
		self.sysTree.setEnabled(False)
		self.sysTree.setRootIsDecorated(False)
		self.sysTree.header().setStretchLastSection(False)	
		
		# TreeWidget for remote file system
		self.ftpTree = QtGui.QTreeWidget(self.centralwidget)
		self.ftpTree.setGeometry(QtCore.QRect(10,75,390,400))
		self.ftpTree.setObjectName("ftpTree")
		self.ftpTree.setEnabled(False)
		self.ftpTree.setRootIsDecorated(False)
		self.ftpTree.setHeaderLabels(['Name', 'Size', 'Owner', 'Group', 'Premission']);
		self.ftpTree.header().setStretchLastSection(False)
		
		# Predefined icons
		self.folderIconBlue = QtGui.QIcon("icon/folder1.png")
		self.fileIconBlue = QtGui.QIcon("icon/file1.png")
		self.folderIconGreen = QtGui.QIcon("icon/folder2.png")
		self.fileIconGreen = QtGui.QIcon("icon/file2.png")
		
		# Download/Upload PorgressBar
		self.progressBar = QtGui.QProgressDialog(self.centralwidget)
		self.progressBar.setGeometry(QtCore.QRect(200,200,200,50))
		self.progressBar.setObjectName("progressBar")
		self.progressBar.setMaximum(100)
		self.progressBar.setMinimum(0)
		self.moveToCenter(self.progressBar)	
		
		# MessageBox for welcome message
		self.msgBox = QtGui.QMessageBox(self.centralwidget)
		self.msgBox.setGeometry(QtCore.QRect(200,200,400,300))
		self.msgBox.setObjectName("msgBox")
		self.msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
		self.moveToCenter(self.msgBox)	
		
		# MessageBox for About
		self.aboutMsg = QtGui.QMessageBox(self.centralwidget)
		self.aboutMsg.setGeometry(QtCore.QRect(200,200,400,300))
		self.aboutMsg.setObjectName("aboutMsg")
		self.aboutMsg.setWindowTitle("Ftp Client 1.0")
		self.aboutMsg.setStandardButtons(QtGui.QMessageBox.Ok)
		self.aboutMsg.setInformativeText(QtGui.QApplication.translate("FtpClientClass", "Ftp Client 1.0\nAuthor: Lin Xin Yu\nE-mail:xinyu0123@gmail.com\nBlog:http://nxforce.blogspot.com", None, QtGui.QApplication.UnicodeUTF8))
		self.moveToCenter(self.aboutMsg)	
		
		FtpClientClass.setCentralWidget(self.centralwidget)
		
		# Menubar & actions
		self.menubar = QtGui.QMenuBar(FtpClientClass)
		self.menubar.setGeometry(QtCore.QRect(0, 0, 600, 22))
		self.menubar.setObjectName("menubar")
		self.menuFile = QtGui.QMenu(self.menubar)
		self.menuFile.setObjectName("menuFile")
		self.menuHelp = QtGui.QMenu(self.menubar)
		self.menuHelp.setObjectName("menuHelp")
		FtpClientClass.setMenuBar(self.menubar)
		self.statusbar = QtGui.QStatusBar(FtpClientClass)
		self.statusbar.setObjectName("statusbar")
		self.statusbar.showMessage('Disconnect')
		FtpClientClass.setStatusBar(self.statusbar)
		self.actionConnect = QtGui.QAction(FtpClientClass)
		self.actionConnect.setObjectName("actionConnect")
		self.actionDisconnect = QtGui.QAction(FtpClientClass)
		self.actionDisconnect.setObjectName("actionDisconnect")
		self.actionQuit = QtGui.QAction(FtpClientClass)
		self.actionQuit.setObjectName("actionQuit")
		self.actionAbout = QtGui.QAction(FtpClientClass)
		self.actionAbout.setObjectName("actionAbout")
		self.menuFile.addAction(self.actionConnect)
		self.menuFile.addAction(self.actionDisconnect)
		self.menuFile.addSeparator()
		self.menuFile.addAction(self.actionQuit)
		self.menuHelp.addAction(self.actionAbout)
		self.menubar.addAction(self.menuFile.menuAction())
		self.menubar.addAction(self.menuHelp.menuAction())

		self.retranslateUi(FtpClientClass)
		
		# Signal & Slot
		QtCore.QObject.connect(self.lineEdit_address,QtCore.SIGNAL("returnPressed()"),FtpClientClass.onReturnPressed)
		QtCore.QObject.connect(self.lineEdit_username,QtCore.SIGNAL("returnPressed()"),FtpClientClass.onReturnPressed)
		QtCore.QObject.connect(self.lineEdit_password,QtCore.SIGNAL("returnPressed()"),FtpClientClass.onReturnPressed)
		QtCore.QObject.connect(self.pushButton_connect,QtCore.SIGNAL("clicked()"),FtpClientClass.onConnect)
		QtCore.QObject.connect(self.sysTree,QtCore.SIGNAL("itemDoubleClicked(QTreeWidgetItem*,int)"),FtpClientClass.onSysItemDoubleClicked)		
		QtCore.QObject.connect(self.ftpTree,QtCore.SIGNAL("itemDoubleClicked(QTreeWidgetItem*,int)"),FtpClientClass.onFtpItemDoubleClicked)
		QtCore.QObject.connect(self.transferMode, QtCore.SIGNAL("currentIndexChanged(int)"), FtpClientClass.onTransferModeChanged)		
		QtCore.QObject.connect(self.actionConnect, QtCore.SIGNAL('triggered(bool)'), FtpClientClass.onConnect)
		QtCore.QObject.connect(self.actionDisconnect, QtCore.SIGNAL('triggered(bool)'), FtpClientClass.onConnect)	
		QtCore.QObject.connect(self.actionQuit, QtCore.SIGNAL('triggered(bool)'), FtpClientClass.close)
		QtCore.QObject.connect(self.actionAbout, QtCore.SIGNAL('triggered(bool)'), FtpClientClass.onAboutTriggered)							
		QtCore.QMetaObject.connectSlotsByName(FtpClientClass)        
	
	# Text Translation for international format - UTF8
	def retranslateUi(self, FtpClientClass):
		FtpClientClass.setWindowTitle(QtGui.QApplication.translate("FtpClientClass", "Ftp Client", None, QtGui.QApplication.UnicodeUTF8))
		self.label_address.setText(QtGui.QApplication.translate("FtpClientClass", "Ftp Host:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_port.setText(QtGui.QApplication.translate("FtpClientClass", "Port:", None, QtGui.QApplication.UnicodeUTF8))		
		self.lineEdit_port.setText(QtGui.QApplication.translate("FtpClientClass", "21", None, QtGui.QApplication.UnicodeUTF8))			
		self.label_username.setText(QtGui.QApplication.translate("FtpClientClass", "Account:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_password.setText(QtGui.QApplication.translate("FtpClientClass", "Password:", None, QtGui.QApplication.UnicodeUTF8))
		self.pushButton_connect.setText(QtGui.QApplication.translate("FtpClientClass", "Connect", None, QtGui.QApplication.UnicodeUTF8))
		self.progressBar.setWindowTitle(QtGui.QApplication.translate("FtpClientClass", "File Transfer", None, QtGui.QApplication.UnicodeUTF8))
		self.menuFile.setTitle(QtGui.QApplication.translate("FtpClientClass", "File", None, QtGui.QApplication.UnicodeUTF8))
		self.menuHelp.setTitle(QtGui.QApplication.translate("FtpClientClass", "Help", None, QtGui.QApplication.UnicodeUTF8))	
		self.actionConnect.setText(QtGui.QApplication.translate("FtpClientClass", "Connect", None, QtGui.QApplication.UnicodeUTF8))
		self.actionDisconnect.setText(QtGui.QApplication.translate("FtpClientClass", "Disconnect", None, QtGui.QApplication.UnicodeUTF8))
		self.actionQuit.setText(QtGui.QApplication.translate("FtpClientClass", "Quit", None, QtGui.QApplication.UnicodeUTF8))
		self.actionAbout.setText(QtGui.QApplication.translate("FtpClientClass", "About", None, QtGui.QApplication.UnicodeUTF8))		
	
	# widget alignment	
	def moveToCenter(self, widget):
		screen = QtGui.QDesktopWidget().screenGeometry()
		size =  widget.geometry()
		widget.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)

############### Ui_FtpClientClass end ################################
