#! /usr/bin/python

######################################################################
#
#      Project: FtpClient
#      Version: 1.0
#         Date: Sat Jul 10 00:00:00 2010
#       Author: Lin Xin Yu
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: 
#               A PyQt based ftp client program
#
######################################################################

############### import modules #######################################

import Ui_FtpClientClass
import os
import sys
import ftplib
import FileTransfer
from PyQt4 import QtCore, QtGui, QtNetwork

############### FtpClientClass Start #################################

class FtpClientClass(QtGui.QMainWindow):
	def __init__(self, parent = None):
		QtGui.QMainWindow.__init__(self, parent)
		self.ui = Ui_FtpClientClass.Ui_FtpClientClass()
		self.ui.setupUi(self)
		
		self.username = None
		self.password = None
		self.address = None
		self.port = 21
		self.ftp = None
		self.passiveMode = True
		
		# record current root location for local filelist
		# default path is your 'Dekstop'
		self.sysRootPath = QtCore.QDir.homePath()+"/Desktop"
		
		# Update treewdiget for local filelist
		self.updateSysFileList()			
	
	#=== SLOT for returnpressed signal ===
	def onReturnPressed(self):
		self.connectToHost()
	
	#=== SLOT for connect/disconnect button ===
	def onConnect(self):
		if(self.ftp == None):
			self.connectToHost()			
		else:
			self.disconnectFromHost()
	
	#=== connect ===	
	def connectToHost(self):		
		self.address = str(self.ui.lineEdit_address.text())
		self.username = str(self.ui.lineEdit_username.text())
		self.password = str(self.ui.lineEdit_password.text())
		self.port = str(self.ui.lineEdit_port.text())
		
		#self.ui.lineEdit_port.emit(QtCore.SIGNAL("returnPressed()"), None)			
		
		# user should at least enter the ftp host 
		if(self.address == ''):
			self.ui.statusbar.showMessage('Please enter ftp host address!')
			self.ui.lineEdit_address.setFocus()
			return False
		
		# username & password are not required for anonymous server		
		
		#if(self.username == ''):
		#	self.ui.statusbar.showMessage('Please enter username!')
		#	self.ui.lineEdit_username.setFocus()
		#	return False
		
		#if(self.password == ''):
		#	self.ui.statusbar.showMessage('Please enter password!')
		#	self.ui.lineEdit_password.setFocus()
		#	return False
		
		# Connect to ftp host		
		self.ftp = ftplib.FTP(self.address)
		
		# set tranfer mode		
		self.ftp.set_pasv(self.passiveMode)
		
		# login
		self.ftp.login(self.username, self.password)

		# Show welcome message
		self.ui.msgBox.setWindowTitle(self.address)
		self.ui.msgBox.setInformativeText(QtGui.QApplication.translate("FtpClientClass", self.ftp.getwelcome(), None, QtGui.QApplication.UnicodeUTF8))
		self.ui.msgBox.show()
		
		self.ui.statusbar.showMessage('Connect to '+self.address+' ...')
		
		# update remote filelist		
		self.updateFtpFileList()
		
		self.ui.sysTree.setEnabled(True)
		self.ui.ftpTree.setEnabled(True)
		
		self.ui.transferMode.setEnabled(False)
		self.ui.lineEdit_address.setEnabled(False)
		self.ui.lineEdit_port.setEnabled(False)
		self.ui.lineEdit_username.setEnabled(False)
		self.ui.lineEdit_password.setEnabled(False)
		self.ui.pushButton_connect.setText('Disconnect')		
		
		return True
	
	#=== disconnect ===		
	def disconnectFromHost(self):
		# close ftp
		self.ftp.close()
		
		# empty object
		self.ftp = None
		
		
		self.ui.sysTree.setEnabled(False)
		self.ui.ftpTree.setEnabled(False)
		
		self.ui.transferMode.setEnabled(True)
		self.ui.lineEdit_address.setEnabled(True)
		self.ui.lineEdit_port.setEnabled(True)
		self.ui.lineEdit_username.setEnabled(True)
		self.ui.lineEdit_password.setEnabled(True)
		self.ui.pushButton_connect.setText('Connect')
		self.ui.statusbar.showMessage('Disconnected from '+self.address+' ...')
	
	#=== upload ===	
	def upload(self, filename, fullname):
		# open file for reading
		fileout = open(fullname, "rb")
		
		# get filesize
		statinfo = os.stat(fullname)
		filesize = statinfo.st_size
		
		# create a FileTransfer object for displaying upload progress
		tp = FileTransfer.TransferProgress(self.ui.progressBar, filename, filesize, 'Upload: ')
		
		# send data to server
		self.ftp.storbinary("STOR " + filename, fileout, 1024,  tp.uploadProgress)
		
		# close file
		fileout.close()
	
	#=== download ===	
	def download(self, filename, filesize):
		# open file for writing
		filein = open(self.sysRootPath+'/'+filename, "wb")
		
		# create a FileTransfer object for displaying download progress
		tp = FileTransfer.TransferProgress(self.ui.progressBar, filename, filesize, 'Download: ', filein)
		
		# get data from server
		self.ftp.retrbinary("RETR " + filename, tp.downloadProgress, 1024)
		
		# close file
		filein.close()	
	
	
	#=== Update treewidget for remote filelist ===	
	def updateFtpFileList(self):
		# clear treewidget
		self.ui.ftpTree.clear()
		
		# get filelist from server
		data = []
		self.ftp.dir(data.append)

		self.flagParentDir = False
		
		# re-format the structure and send into ftpTree 
		for line in data:
			spline = line.split()
			if(spline[8][0] != '.' and self.flagParentDir == False):
				item = QtGui.QTreeWidgetItem()
				item.setText(0, '..')
				item.setText(1, '1024')
				item.setText(2, spline[2])
				item.setText(3, spline[3])
				item.setText(4, spline[0])
				item.setIcon(0, self.ui.folderIconBlue)
				self.ui.ftpTree.addTopLevelItem(item)
				self.flagParentDir = True
			else:
				self.flagParentDir = True
				
			item = QtGui.QTreeWidgetItem()
			item.setText(0, spline[8])
			item.setText(1, spline[4])
			item.setText(2, spline[2])
			item.setText(3, spline[3])
			item.setText(4, spline[0])
			
			if(spline[0][0] == 'd'):
				item.setIcon(0, self.ui.folderIconBlue)
			else:
				item.setIcon(0, self.ui.fileIconBlue)
				
			self.ui.ftpTree.addTopLevelItem(item)
	
	
	#=== Update treewidget for local filelist ===	
	def updateSysFileList(self):
		# clear treewidget
		self.ui.sysTree.clear()
		
		# get local directory path
		sysDir = QtCore.QDir(self.sysRootPath)
		sysDir.setFilter(QtCore.QDir.Files | QtCore.QDir.Dirs | QtCore.QDir.NoSymLinks)
		sysDir.setSorting(QtCore.QDir.DirsFirst | QtCore.QDir.Name)	
	
		# retrive the file info and pack into sysTree
		fileList = sysDir.entryInfoList();
		for i in range(0,len(fileList)):
			fileInfo = fileList[i]
			item = QtGui.QTreeWidgetItem()
			item.setText(0, fileInfo.fileName())
			item.setText(1, str(fileInfo.size()/1024)+' KB')
			item.setText(2, fileInfo.owner())
			item.setText(3, fileInfo.group())
			item.setText(4, fileInfo.filePath())
			
			if(fileInfo.isDir()):
				item.setIcon(0, self.ui.folderIconGreen)
			else:
				item.setIcon(0, self.ui.fileIconGreen)
	
			self.ui.sysTree.addTopLevelItem(item)
	
	#=== SLOT for ftpTree ===
	def onFtpItemDoubleClicked(self, item):
		permission = str(item.text(4))
		if(permission[0] == 'd'):
			self.ftp.cwd(str(item.text(0)))
			self.updateFtpFileList()
		else:
			self.download(str(item.text(0)), str(item.text(1)))
			
	#=== SLOT for sysTree ===		
	def onSysItemDoubleClicked(self, item):
		path = str(item.text(4))
		if(QtCore.QFileInfo(path).isDir()):
			self.sysRootPath = path
			self.updateSysFileList()
		else:
			self.upload(str(item.text(0)) ,path)
	
	#=== SLOT for transfer mode combobox ===		
	def onTransferModeChanged(self, index):
		if(index == 0):
			self.passiveMode = True
		else:
			self.passiveMode = False
	
	#=== SLOT for menubar's actions ===
	def onAboutTriggered(self):
		self.ui.aboutMsg.show()
		
	#=== SLOT for close window ===
	def closeEvent(self, event):
		if(self.ftp != None):
			self.ftp.close()


############### FtpClientClass End ###################################

