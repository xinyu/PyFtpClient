#! /usr/local/bin/python

###############################################################
# Project: Python Ftp Client
# Filename: FileTransfer.py
# Description: A TransferProgress class for showing the downloading
#					or uploading progress.
# Date: 2009.05.21
# Programmer: Lin Xin Yu
# E-mail: xinyu0123@gmail.com
# Website:http://nxforce.blogspot.com
###############################################################

#================= TransferProgress Class Start ===============

class TransferProgress:
	def __init__(self, pBar, filename, filesize, msg, file=None):
		
		# file descriptor will be used while downloding		
		self.file = file
		
		# cause the value of TreeWidgetItem are all string, so typecast it		
		self.totalSize = int(filesize)
		self.recvSize = 0
		
		# QProgressBar
		self.pBar = pBar
		self.pBar.setWindowTitle(msg+filename)
		self.pBar.show()
		
	def uploadProgress(self, data):
		self.recvSize += len(data)
		self.pBar.setValue(self.recvSize*100 /self.totalSize)
		
	def downloadProgress(self, data):
		self.recvSize += len(data)
		self.file.write(data)
		self.pBar.setValue(self.recvSize*100 /self.totalSize)

#================= TransferProgress Class End =================
